/*
 * Copyright 1999-2004 Carnegie Mellon University.
 * Portions Copyright 2004 Sun Microsystems, Inc.
 * Portions Copyright 2004 Mitsubishi Electric Research Laboratories.
 * All Rights Reserved.  Use is subject to license terms.
 *
 * See the file "license.terms" for information on usage and
 * redistribution of this file, and for a DISCLAIMER OF ALL
 * WARRANTIES.
 *
 */

package com.voice.jarvis;

import edu.cmu.sphinx.frontend.util.Microphone;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import java.io.IOException;
import edu.cmu.sphinx.util.props.PropertyException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;


public class App {

    static final DefaultHttpClient httpClient;

    static {    
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 1000);
        HttpConnectionParams.setSoTimeout(httpParameters, 1000);
        httpClient = new DefaultHttpClient(httpParameters);
    }

    public static void main(String[] args) throws IOException, PropertyException, InstantiationException {
        ConfigurationManager cm;
        
        if (args.length > 0) {
            cm = new ConfigurationManager(args[0]);
        } else {
            cm = new ConfigurationManager(App.class.getResource("helloworld.config.xml"));
        }
        
        Recognizer recognizer = (Recognizer) cm.lookup("recognizer");
        recognizer.allocate();
        
        // start the microphone or exit if the programm if this is not possible
        Microphone microphone = (Microphone) cm.lookup("microphone");
        if (!microphone.startRecording()) {
            System.out.println("Cannot start microphone.");
            recognizer.deallocate();
            System.exit(1);
        }
        // loop the recognition until the programm exits.
        while (true) {
            Result result = recognizer.recognize();
            System.out.println(result.getBestFinalToken());
            if (result != null && result.getBestFinalToken() != null) {
                String resultText = result.getBestFinalResultNoFiller();
                if ( isPresent("jarvis", resultText) == true) {
                    System.out.println("You said: " + resultText + '\n');
                    sender(resultText);
                }
                
            } else {
                System.out.println("Could not find word, starting external search...");
            }
        }
    }
    
    static void sender(String input) {
        try {
           System.out.println("Inside sender call before call");
            
           //HttpClient client = new DefaultHttpClient();
           HttpPost post = new HttpPost("http://localhost:3000/jarvis");
           JSONObject msg = new JSONObject(); 
           msg.put("data", input);
           
           System.out.println("POSTING" + msg.toString());

           HttpEntity entity = new StringEntity(msg.toString(), ContentType.create("application/json"));
           post.setEntity(entity);

           BufferedReader reader = new BufferedReader(new InputStreamReader(httpClient.execute(post).getEntity().getContent()));
           
           String response = reader.readLine();
           
           System.out.println(response);
        }
        catch(IOException e)
        { 
            System.out.println(e.getMessage());
        } catch (JSONException e) {
            System.out.println(e.getMessage());
        }
    }
    
    static boolean isPresent(String query, String s) {    
        String [] deli = s.split("[.\\s,?;]+");
        for (String deli1 : deli) {
            if (query.equals(deli1)) {
                return true;
            }
        }

        return false;    
    }
    
}
